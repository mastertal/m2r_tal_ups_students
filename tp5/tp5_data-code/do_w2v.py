# construct word2vec model using gensim

# we want use gensim's Word2Vec module
from gensim.models import Word2Vec 

import gzip
import logging

# set up logging for gensim
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',
                    level=logging.INFO)

# we define a PlainTextCorpus class; this will provide us with an
# iterator over the corpus (so that we don't have to load the corpus
# into memory)
class PlainTextCorpus(object):
    def __init__(self, fileName):
        self.fileName = fileName

    def __iter__(self):
        for line in gzip.open(self.fileName, 'rt', encoding='utf-8'):
            yield  line.split()

# instantiate the corpus class using corpus location
sentences = PlainTextCorpus('data/corpus_food.txt.gz')

# we only take into account words with a frequency of at least 50, and
# we iterate over the corpus only once
model = Word2Vec(sentences, min_count=50, iter=1)

# finally, save the constructed model to disk
model.save('model_word2vec_food')
