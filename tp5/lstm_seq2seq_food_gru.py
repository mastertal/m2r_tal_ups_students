#Encoder-decoder model for recipe generation

from __future__ import print_function

from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, LSTM, Dense, Embedding, GRU
from tensorflow.keras.optimizers import SGD, RMSprop
import numpy as np
from gensim.models import Word2Vec

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

batch_size = 128  # Batch size for training
epochs = 20  # Number of epochs to train for
latent_dim = 50  # Dimensionality of the embeddings and hidden representation
# Path to the data and vocabulary file
data_path = 'data/sents_recipes.txt'
voc_path = 'data/vocab.1000'

#############
# Load data #
#############

# Load vocabulary as mappings (index2word & word2index)
# These mappings will be used in order to map words to integers and
# vice versa
i2w, w2i = [], {}
vocFile = open(voc_path, 'r', encoding="utf-8")
for line in vocFile:
    line = line.strip()
    i2w.append(line)
    w2i[line] = len(i2w) - 1

# Load training sequences
input_texts = []
target_texts = []
with open(data_path, 'r', encoding='utf-8') as f:
    for line in f:
        line = line.strip()
        input_text, target_text = line.split('\t')
        input_text = input_text.split(' ')
        target_text = target_text.split(' ')
        input_text = [i if i in w2i else '<unk>' for i in input_text]
        target_text = [i if i in w2i else '<unk>' for i in target_text]
        # Prepend "start sequence" token &
        # append "end sequence" token
        target_text = ['<start>'] + target_text + ['<stop>']
        input_texts.append(input_text)
        target_texts.append(target_text)

num_vocabulary = len(i2w)
max_encoder_seq_length = max([len(txt) for txt in input_texts])
max_decoder_seq_length = max([len(txt) for txt in target_texts])

print('Number of samples:', len(input_texts))
print('Number of unique tokens:', num_vocabulary)
print('Max sequence length for inputs:', max_encoder_seq_length)
print('Max sequence length for outputs:', max_decoder_seq_length)

#################
# Preprocessing #
#################

# Recipe data is in text form - we need to turn it into numbers such
# that our neural network architecture is able to process it.

encoder_input_data = np.zeros(
    (len(input_texts), max_encoder_seq_length),
    dtype='int32')
decoder_input_data = np.zeros(
    (len(input_texts), max_decoder_seq_length),
    dtype='int32')
decoder_target_data = np.zeros(
    (len(input_texts), max_decoder_seq_length, num_vocabulary),
    dtype='float32')

for i, (input_text, target_text) in enumerate(zip(input_texts, target_texts)):
    for t, word in enumerate(input_text):
        encoder_input_data[i, t] = w2i[word]
    for t, word in enumerate(target_text):
        decoder_input_data[i, t] = w2i[word]
        if t > 0:
            # decoder_target_data will be ahead by one timestep
            # and will not include the start token.
            decoder_target_data[i, t - 1, w2i[word]] = 1.


## w2vmodel
m_w2v = Word2Vec.load('model_word2vec_food_sg_s50_w5')
#m_w2v = Word2Vec.load('model_word2vec_food')
m_init = np.random.uniform(low=-0.05,high=0.05, size=(1000,50))
for i,w in enumerate(i2w):
    if w in m_w2v.wv:
        m_init[i] = m_w2v.wv[w]
    else:
        print(w)



####################
# Model definition #
####################

# define embedding layer, shared between encoder and decoder
embedding_layer = Embedding(num_vocabulary, latent_dim, weights=[m_init])

# Define an input sequence -> embedding -> lstm encoder
encoder_inputs = Input(shape=(None,))
embedded1 = embedding_layer(encoder_inputs)
encoder = GRU(latent_dim, return_state=True)
encoder_outputs, state_h = encoder(embedded1)
# 'encoder_states' will be used to initialize the decoder
encoder_states = state_h

# Define and input sequence -> embedding -> lstm decoder
# Note that we have an input sequence because we use the training
# corpus data as input
decoder_inputs = Input(shape=(None,))
embedded2 = embedding_layer(decoder_inputs)
decoder_lstm = GRU(latent_dim, return_sequences=True, return_state=True)
decoder_outputs, _ = decoder_lstm(embedded2,
                                     initial_state=encoder_states)
decoder_dense = Dense(num_vocabulary, activation='softmax')
decoder_outputs = decoder_dense(decoder_outputs)

# Define the model that will turn
# `encoder_input_data` & `decoder_input_data` into `decoder_target_data`
model = Model([encoder_inputs, decoder_inputs], decoder_outputs)

############
# Training #
############

optim = SGD(lr=0.1, momentum=0.9)
model.compile(optimizer=optim, loss='categorical_crossentropy')
model.fit([encoder_input_data, decoder_input_data], decoder_target_data,
          batch_size=batch_size,
          epochs=epochs,
          validation_split=0.2)
# Save model
model.save('s2s.h5')

#############
# Inference #
#############

# Define a specific model for inference
encoder_model = Model(encoder_inputs, encoder_states)

decoder_state_input_h = Input(shape=(latent_dim,))
#decoder_state_input_c = Input(shape=(latent_dim,))
decoder_states_inputs = decoder_state_input_h
decoder_outputs, state_h = decoder_lstm(
    embedded2, initial_state=decoder_states_inputs)
decoder_states = state_h
decoder_outputs = decoder_dense(decoder_outputs)
decoder_model = Model(
    [decoder_inputs] + [decoder_states_inputs],
    [decoder_outputs] + [decoder_states])

# function to sample from a probability distribution parameter
# 'temperature' influences how much randomness we want (low
# temperature = low variation)
def sample(preds, temperature=0.6):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    exp_preds[1] = 1e-15
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)

# function that decodes an input sequence, generating the next sentence
# of the recipe
def decode_sequence(input_seq):
    states_value = encoder_model.predict(input_seq)
    target_seq = np.zeros((1, 1), dtype='int32')
    target_seq[0, 0] = w2i['<start>']
    stop_condition = False
    decoded_sentence = []
    while not stop_condition:
        output_tokens, h = decoder_model.predict(
            [target_seq] + [states_value])
        sampled_token_index = sample(output_tokens[0, -1, :])
        sampled_char = i2w[sampled_token_index]
        decoded_sentence.append(sampled_char)
        if (sampled_char == '<stop>' or
           len(decoded_sentence) > max_decoder_seq_length):
            stop_condition = True
        target_seq = np.zeros((1, 1), dtype='int32')
        target_seq[0, 0] = sampled_token_index
        states_value = h
    return decoded_sentence

def generate_next_sentence(input_sent):
   input_list = input_sent.split(' ')
   input_seq = [w2i[w] if w in w2i else 1 for w in input_list]
   decoded_sentence = decode_sequence(input_seq)
   return decoded_sentence


# See whether our model gives sensible results using 10 instances from
# our training set
for seq_index in range(10):
    # Take one sequence (part of the training set)
    # for trying out decoding.
    input_seq = encoder_input_data[seq_index: seq_index + 1]
    decoded_sentence = decode_sequence(input_seq)
    print('-')
    print('Input sentence:', input_texts[seq_index])
    print('Decoded sentence:', decoded_sentence)
