Municipales à Paris : Anne Hidalgo se lance à son tour dans la bataille

Publié le 11/01/2020 à 20:03

Fin décembre, elle ne cachait pas son impatience à descendre dans l'arène, et confiait à l'AFP avoir "hâte". Pour autant, l'élue socialiste est la dernière à se lancer, suivant jusque-là à distance et avec une certaine gourmandise les premiers pas parfois hasardeux de ses concurrents, notamment à LREM, minée par la rivalité interne entre Benjamin Griveaux et Cédric Villani. 

"Oui. Je suis candidate à un nouveau mandat de maire de Paris. Cette ville que j'aime, qui me passionne, à laquelle je consacre une grande partie de ma vie", souligne Mme Hidalgo samedi dans les colonnes du Parisien, mettant fin à un secret de polichinelle.

"Je souhaite poursuivre l'action engagée en 2001 et amplifiée en 2014. C'est désormais une course contre la montre. Nous avons dix ans pour agir face à l'urgence climatique", ajoute celle qui fut l'adjointe de Bertrand Delanoë avant de lui succéder dans une ville où la gauche est au pouvoir depuis dix-neuf ans. 

Elle assure aussi qu'elle ne sera "pas candidate à la présidentielle de 2022" si elle est réélue, alors que certains adversaires la soupçonnent de vouloir se servir de Paris comme d'un tremplin pour l'Elysée. 

Pour le scrutin municipal, annoncé plus incertain que jamais par les analystes, Mme Hidalgo, 60 ans, fait pour l'instant la course en tête. Dans un sondage Ifop commandé par l'équipe de Cédric Villani, elle devance, avec 22,5% des intentions de vote, le candidat LREM Benjamin Griveaux (17%), la LR Rachida Dati (17%), le dissident Cédric Villani (14%) et l'écologiste David Belliard (12,5%). 

Candidate jusqu'ici dans le XVe arrondissement, elle sera cette fois en deuxième position dans le XIe arrondissement, où se présente également son allié de la majorité sortante, le candidat EELV David Belliard. Un choix qui n'est pas du goût de l'entourage de l'écologiste.

Faire la différence d'entrée

Autre nouveauté: celle qui est toujours socialiste, est soutenue cette fois par la plateforme "Paris en Commun" qui englobe socialistes, élus de Générations, communistes et personnalités de la société civile dont l'ancien président du Samu social Eric Pliez (candidat à la mairie du XXe arrondissement) et l'ancienne journaliste Audrey Pulvar (sur les listes de Paris Centre). 

L'élue qui ne s'étend par sur d'éventuelles alliances au deuxième tour, entend proposer le rassemblement le plus large possible.  

Du côté du PS, on ne s'offusque pas de cette nouvelle étiquette. "Elle ne veut pas donner le sentiment qu'elle est la femme d'un appareil ou d'un parti politique", analyse auprès de l'AFP le Premier secrétaire du PS, Olivier Faure, qui lui demande seulement "de maintenir cette ville à gauche". 

"La campagne sera courte et dynamique. Notre volonté, c'est de faire la différence dès notre entrée en campagne", insiste auprès de l'AFP le premier secrétaire de la fédération socialiste de Paris, Rémi Féraud. 

Les premières sorties de Mme Hidalgo seront dans le nord-est de la capitale, des arrondissements populaires dont les votes sont convoités par tous les candidats. 

Lundi soir, elle se rendra auprès des militants et des élus réunis au sein de "Paris en Commun".  
Avant la confrontation des idées, ses opposants comptent attaquer son bilan. Le candidat de centre-droit Pierre-Yves Bournazel dénonce un mandat marqué par "une dégradation de la vie quotidienne des Parisiens et une gouvernance trop centralisée". La candidate LR dans le XIVe arrondissement, Marie-Claire Carrère-Gée, fustige "une gestion dispendieuse avec une dette de 6 milliards d'euros". 

"Anne Hidalgo met fin à un suspense insoutenable, ironise de son côté Benjamin Griveaux. Elle va pouvoir proposer pendant ces deux prochains mois des idées pour résoudre des problèmes qu'elle n'a pas réglé voire qu'elle a elle-même créé au cours des six dernières années". 