#!/usr/bin/env python

import nltk

from nltk.corpus import PlaintextCorpusReader
from nltk.collocations import BigramCollocationFinder
from nltk.collocations import BigramAssocMeasures
from nltk.tokenize.simple import SpaceTokenizer, LineTokenizer

bigram_measures = BigramAssocMeasures()

c = PlaintextCorpusReader('corpus/tweet',
                          '.*',
                          word_tokenizer=SpaceTokenizer(),
                          sent_tokenizer=LineTokenizer())

finder = BigramCollocationFinder.from_words(c.words())
finder.apply_freq_filter(5)

print(finder.nbest(bigram_measures.raw_freq, 20))
