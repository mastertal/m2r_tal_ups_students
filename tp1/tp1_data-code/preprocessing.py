#!/usr/bin/env python

import spacy

# Load French preprocessing models
nlp = spacy.load('fr')

# Read in string of characters
with open('corpus/lemonde.txt') as inFile:
    text = inFile.read()

# Preprocess using spacy's pipeline
doc = nlp(text)

# inspect tokens, lemmas, and pos tags
for token in doc:
    print(token.text, token.lemma_, token.pos_)
