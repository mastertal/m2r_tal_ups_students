#!/usr/bin/env python

import random
import glob
from collections import Counter

class BigramModel:
    def __init__(self, training_sentences):
        self.BOUNDARY_TOKEN = '</s>'
        self.estimate_bigram_probabilities(training_sentences)
        
    def estimate_bigram_probabilities(self, training_sentences):
        self.probs = {}
        bigramDictionary = {}
        for sent in training_sentences:
            #Boundary tokens are added at beginning and end of sentence
            newSent = [self.BOUNDARY_TOKEN] + sent + [self.BOUNDARY_TOKEN]
            #Traverse each sentence, recording the counts for each
            #current word given the previous word (context). You will need a
            #dictionary of dictionaries

            #INSERT CODE HERE
            
        #Normalize the counts for each context, so that they sum to
        #1. This turns each context in a proper probability
        #distribution p(w_n+1|w_n)

        #INSERT CODE HERE
                
    def generateSentence(self):
        context = [self.BOUNDARY_TOKEN]
        result = []
        w = None
        while not w == self.BOUNDARY_TOKEN:
            r = random.random() # returns a random float between 0 and 1
            x = 0
            c = self.probs[" ".join(context)] # this will be a Counter
            for k,v in c.items():
                x = x + v
                if x > r: # choose this word
                    w = k
                    result.append(w)
                    context = [w]
                    break
        result = result[:-1] # drop the boundary token
        return result
        

#####
    
corpus = []
for fn in glob.glob('corpus/tweet/*'):
    with open(fn, 'rt') as inFile:
        for line in inFile:
            line = line.strip()
            corpus.append(line.split(' '))

m = BigramModel(corpus)
#print(m.generateSentence())
